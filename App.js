/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';  
import { Alert, AppRegistry, Button, StyleSheet, View, Image } from 'react-native';  
import TM from 'rn-epson-tm82';



export default class App extends Component {  
    onPressButton = async() => {  
      try{
        await TM.initialize();
        await TM.writeText("My Restaurant Cafe\n");
        await TM.writeText("------------------------------------------------\n");
        const column = [4, 26, 9, 9];
        const align = [2, 0, 2, 2];
        await TM.printColumn(column, align, [`Qty`, "Item Name", "Price", "Amount"], {});
        await TM.writeText("------------------------------------------------\n");
        await TM.printColumn(column, align, [`1`, "Rice", "$1.0", "$1.0"], {});
        await TM.writeText("------------------------------------------------\n");
        const totalColumn = [30,18];
        const totalAlign = [0,2];
        await TM.printColumn(totalColumn,totalAlign,["Total","$21.0"],{});
        await TM.writeFeed(2);
        await TM.writeText("Thank you. Please come back again");
        await TM.startPrint("192.168.100.13");
      } catch (error) {
        console.error(error);
      }
      Alert.alert('You clicked the button!')  
    }  
    

    render() {  
        return (  
          <View style={styles.container}>  
            <View style={styles.buttonContainer}>  
              <Button  
                  onPress={this.onPressButton}  
                  title="Press Me"  
              />  
            </View>  
          </View>
        );  
    }  
}  
  
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
}); 